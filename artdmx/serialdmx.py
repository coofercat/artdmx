
from serial import Serial,PARITY_NONE,STOPBITS_TWO,EIGHTBITS
import time

class SerialDMX:
    def __init__(self, config):
        self.config = config
        self.port = config['port']
        self.ser = None
        self.connect()
        self.send_state = 0
        self.send_index = 0
        self._packets_sent = 0

        try:
            if config['frames_per_second'] >= 1:
                self.frames_per_second = config['frames_per_second']
            else:
                raise KeyError
        except KeyError:
            self.frames_per_second = 44
        try:
            if config['frame_length'] > 0 and config['frame_length'] <= 512:
                self.frame_length = config['frame_length']
            else:
                raise KeyError
        except KeyError:
            self.frame_length = 512

        self.actual_fps = self.frames_per_second
        self.inter_frame_time = 1 / self.frames_per_second

        # See https://erg.abdn.ac.uk/users/gorry/eg3576/DMX-frame.html
        self.frame_duration = (92 + 12 + (44 * self.frame_length)) * 1000000

        self.preamble_started_at = time.time()

        new_frame = [0] * self.frame_length
        self.frame = bytes([0] + new_frame)

    def packets_sent(self):
        x = self._packets_sent
        self._packets_sent = 0
        return x

    def set_actual_fps(self, fps):
        self.actual_fps = fps

        multiplier = fps / self.frames_per_second

        if multiplier < 0.999 or multiplier > 1.001:
            # running too slow or too fast
            self.inter_frame_time = self.inter_frame_time * multiplier


    def connect(self):
        self.ser = Serial(
            port=self.port,
            baudrate=19200,
            parity=PARITY_NONE,
            stopbits=STOPBITS_TWO,
            rtscts=False,
            dsrdtr=True,
            exclusive=True,
            write_timeout=100,
        )
        return self.ser

    def set_frame(self, new_frame):
        diff = len(new_frame) - self.frame_length 
        if diff < 0:
            new_frame = new_frame + [] * (0 - diff)
        elif diff > 0:
            new_frame = new_frame[0:diff]
        self.frame = bytes([0] + new_frame)

    # This trick from https://gist.github.com/pingswept/3921409
    def send_preamble(self):
        self._packets_sent = self._packets_sent + 1

        # Timing: We require an 8uS 'mark after break', which will be formed
        # of our two stop bits, so 4uS per bit. We'll send a total of 10 bits,
        # so that means 40uS per byte. 1/40uS = 25000

        self.ser.baudrate = 19200
        #self.ser.baudrate = 25000
        x = time.time() - self.preamble_started_at
        #print("Preamble at %s" % (x))
        t = 0.045
        if x < t:
            time.sleep(t - x)
        self.preamble_started_at = time.time()
        self.ser.write((chr(0)).encode())

    def send_frame(self):
        self.ser.baudrate = 250000
        self.ser.write(self.frame)

    def fileno(self):
        return self.ser.fileno()

    def flush(self):
        return self.ser.flush()

    def send(self):
        if self.ser.out_waiting != 0:
            print("%s Rejecting send as waiting..." % (self.port))
            return
        if self.send_state == 0:
            self.send_preamble()
            self.send_state = 1
        elif self.send_state == 1:
            self.send_frame()
            self.send_state = 0

    def ready_to_send(self):
        time_since_last_send = time.time() - self.preamble_started_at

        if self.send_state == 1:
            # Just sent pre-amble, so can go as soon as clear
            if self.ser.out_waiting:
                diff = 0.00040 - time_since_last_send
                if diff > 0:
                    #print("diff = %s" % (diff))
                    return  diff
            return 0

        
        diff = self.inter_frame_time - time_since_last_send

        if diff > 0:
            # Still sending the previous packet
            return diff
        else:
            # We're past the end of the last packet
            if self.ser.out_waiting:
                # In theory this shouldn't happen, but it does - a lot
                # I think it's because we don't send the frame immediately
                # behind the preamble.
                return 0.001
            # We're ready to send another packet

        return 0
