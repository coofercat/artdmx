import time
from select import select
import os
import json
import sys

from artdmx.serialdmx import SerialDMX
from artdmx.utils import *

class TalkToParent:
    def __init__(self, pipe):
        self.to_send_to_parent = []
        self.pipe = pipe

    def fileno(self):
        return self.pipe.fileno()

    def send(self):
        try:
            line = self.to_send_to_parent.pop(0)
            self.pipe.write("%s\n" % (line))
            self.pipe.flush()
        except IndexError:
            pass

    def is_waiting(self):
        if len(self.to_send_to_parent) > 0:
            return True
        return False

    def queue(self, thing):
        self.to_send_to_parent.append(thing)


def worker(universe, universe_config, from_parent, to_parent):
    scoreboard = ScoreBoard()
    parent = TalkToParent(to_parent)

    parent.queue("INFO: Universe %d opening port %s" % (universe, universe_config['port']))
    ser = SerialDMX(universe_config)
    scoreboard.set('serial port open time', time.time())
    buf = Buffering()
    get_fps_time = int(time.time()) + 5

    packet_count_start_time = time.time()
    while(1):
        writers = []

        if parent.is_waiting():
            writers.append(parent)

        timeout = ser.ready_to_send()
        if timeout == 0:
            writers.append(ser)

        (readable, writable, errored) = select([from_parent], writers, [], timeout)
        for item in writable:
            item.send()

        for item in readable:
            data = os.read(item.fileno(), 4096)
            if len(data) == 0:
                # Parent has closed the connection - can't continue
                sys.exit(0)
            try:
                buf.add(data.decode('utf-8'))
            except BufferOverFlow:
                print("Buffer overflow in worker")
                print("Buffer: >%s<" % (buf.buffer))

        while(1):
            line = buf.get()
            if line:
                if len(line) > 5 and line[0:5] == 'DMX: ':
                    try:
                        dmx = json.loads(line[5:])
                    except json.decoder.JSONDecodeError:
                        # failed to read json - add one to a scoreboard
                        print("")
                        print("Failed to decode: %s" % (line))
                        print("")
                        scoreboard.add('parent json decode failure', 1)
                    ser.set_frame(dmx)
                else:
                    # malformed line from parent - add one to a scoreboard
                    scoreboard.add('parent receive errors', 1)
            else:
                break

        i = int(time.time())
        if i > get_fps_time:
            fps = ser.packets_sent() / (time.time() - packet_count_start_time)
            packet_count_start_time = time.time()
            ser.set_actual_fps(fps)
            scoreboard.set('fps', fps)
            scoreboard.save()

            parent.queue("SCOREBOARD: %s" % (scoreboard))
            get_fps_time = i + 5
