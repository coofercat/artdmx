import time
import json

class ScoreBoard():
    def __init__(self, filename = None):
        self.filename = filename
        self.stats = {}
        self.fileobj = None
        if filename is not None:
            self.open_file()

    def open_file(self):
        try:
            fo = open("%s" % (self.filename), "w+")
            self.fileobj = fo
        except (FileNotFoundError, IsADirectoryError, PermissionError) as e:
            # Scoreboard failed to open - log it and carry on
            self.fileobj = None

    def save(self):
        if self.fileobj is None:
            return
        self.stats['last update time'] = time.time()
        data = json.dumps(self.stats, sort_keys=True, indent=2)
        self.fileobj.seek(0)
        self.fileobj.write(data + '\n')
        self.fileobj.truncate()
        self.fileobj.flush()

    def __str__(self):
        self.stats['last update time'] = time.time()
        return json.dumps(self.stats, sort_keys=True, separators=(',', ':'), indent=None)

    def set(self, thing, value):
        self.stats[thing] = value

    def add(self, thing, amount = 1):
        try:
            self.stats[thing] = self.stats[thing] + amount
        except KeyError:
            self.stats[thing] = amount

class BufferOverFlow(Exception):
    pass

class Buffering:
    def __init__(self, max = 16192):
        self.buffer = ''
        self.max = max
        self.newline_index = None

    def _set_newline_index(self):
        try:
            self.newline_index = self.buffer.index('\n')
        except ValueError:
            self.newline_index = None

    def add(self, data):
        self.buffer = self.buffer + data

        self._set_newline_index()

        if len(self.buffer) > self.max:
            # What to do here? throw it all away, or trim?
            self.buffer = self.buffer[-self.max:]
            self._set_newline_index()
            raise BufferOverFlow

    def get(self):
        if self.newline_index:
            try:
                first_line = self.buffer[0:self.newline_index]
                self.buffer = self.buffer[self.newline_index+1:]
                self._set_newline_index()
                return first_line
            except ValueError:
                # Something wrong - reset the index
                self._set_newline_index()
        return None

