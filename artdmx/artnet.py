import socket

import bitstring

class ArtNet:
    def __init__(self, ip = '0.0.0.0', port = 6454):
        self.ip = ip
        self.port = port
        self.sock = None
        self.listen()

        self.last_sequence_number = 0

    def listen(self):
        self.sock = socket.socket(socket.AF_INET, # Internet
               socket.SOCK_DGRAM) # UDP
        self.sock.bind((self.ip, self.port))

    def fileno(self):
        return self.sock.fileno()

    def receive(self):
        raw_data, addr = self.sock.recvfrom(1024)
        return self.decode(raw_data, addr)

    def decode(self, data, addr):
        try:
            return self._decode(data,addr)
        except bitstring.ReadError:
            # Packet shorter than expected
            return False

    def _decode(self, data, addr):
        header = b'Art-Net\0'
        protocol_version = 14

        b = bitstring.BitStream(bytes=data)
        h = b.read('bytes:8')

        if h != header:
            print("Dropping packet because header not correct: %s" % (h))
            return False

        # Opcode and version next
        opcode = b.read('int:16')
        version = b.read('uintbe:16')

        if version != 14:
            print("Dropping packet because it's not protocol version 14 (is %s)" % (version))
            return False

        seq = b.read('int:8')
        # TODO: check the sequence number is greater than the last (but observe wrap-around)

        if opcode == 80:
            # DMX packet - the kind we like ;-)
            #print("DMX packet")

            physical = b.read('int:8')
            universe = b.read('uintle:16')

            #print("Universe %d" % (universe))

            length = b.read('uintbe:16')
            dmx = b.read('bytes:%d' % length)

            return {'universe': universe, 'framedata': list(dmx)}

        else:
            print("Opcode %s not supported" % (opcode))
            return False

