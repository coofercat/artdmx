import os
import sys
from setproctitle import setproctitle
import yaml
from argparse import ArgumentParser

from artdmx import parent, worker


def start(universes, artnet_config):
    count = 0
    for universe in universes:
        r1, w1 = os.pipe()
        r2, w2 = os.pipe()
        try:
            pid = os.fork()
        except OSError:
            exit("Could not create a child process")

        if pid == 0:
            # child
            print("Launched child at %s" % (os.getpid()))
            setproctitle("artdmx worker universe %d" % (universe))
            os.close(w1)
            os.close(r2)
            r1 = os.fdopen(r1)
            w2 = os.fdopen(w2, 'w')
            try:
                worker(universe, universes[universe], r1, w2)
            except KeyboardInterrupt:
                print("Worker %d quit" % (universe))
            exit("Child %d done" % (os.getpid()))
        else:
            # parent
            os.close(r1)
            os.close(w2)
            w1 = os.fdopen(w1,'w')
            r2 = os.fdopen(r2)
            universes[universe]['pid'] = pid
            universes[universe]['pipe_to'] = w1
            universes[universe]['pipe_from'] = r2

    # Now actually be the master
    setproctitle("artdmx master")
    try:
        parent(universes)
    except KeyboardInterrupt:
        print("Quit")

