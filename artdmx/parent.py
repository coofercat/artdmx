import time
from select import select
import os
import json
import sys

from artdmx.artnet import ArtNet
from artdmx.utils import Buffering


def parent(universes):
    artnet = ArtNet()

    readers = [artnet]
    files_to_universes = {}
    for universe in universes:
        if 'pipe_from' in universes[universe]:
            readers.append(universes[universe]['pipe_from'])
            files_to_universes[universes[universe]['pipe_from'].fileno()] = universe

    buf = Buffering()
    while(1):
        (readable, writable, errored) = select(readers, [], [])
        for item in readable:
            if isinstance(item, ArtNet):
                packet = item.receive()
                try:
                    universe = packet['universe']
                    out = "DMX: " + json.dumps(packet['framedata'], separators=(',', ':'), indent=None) + '\n'
                    universes[universe]['pipe_to'].write(out)
                    universes[universe]['pipe_to'].flush()
                except (KeyError, TypeError):
                    pass
            else:
                # is a worker talking to us
                fn = item.fileno()
                # Get the universe this worker is on
                try:
                    universe = files_to_universes[fn]
                except KeyError:
                    # What?
                    universe = -1
                data = os.read(fn, 4096)
                if len(data) == 0:
                    print("Worker %d has closed connection to us" % (universe))
                    # TODO remove pipe from the readers, or else we'll do this continuously
                    readers.remove(item)
                    if readers == [artnet]:
                        print("All workers have stopped")
                        sys.exit(1)
                else:
                    try:
                        line = buf.add(data.decode('utf-8'))
                    except BufferOverflow:
                        print("Buffer overflow on worker chatter buffer")
                    while(1):
                        line = buf.get()
                        if line:
                            print("Worker %d said: %s" % (universe, line))
                        else:
                            break
