# ArtDMX

An Artnet <-> DMX bridge

# What is it?

It's a 'bridge' between Artnet (as the 'input') and DMX as the output. The DMX
output is 'serial', utilising cheap RS485 converters. It's possible to run multiple
DMX universes with a Rapsberry Pi. The Pi can even run something like QLC+, or else
can just receive Artnet from the network.

# When Would I want to use it?

If you want to run multiple DMX universes, don't want to spend much money and
fancy a bit of a challenge ;-)

I've found that devices like an Entec OpenDMX are tricky to use properly. The likes
of QLC+ and even OLA will happily use it, but can't 'fill' the serial port sufficiently
reliably to avoid flickering. ArtDMX can maintain suitable DMX frame rates to
avoid flickering, and QLC+ can use it via Artnet.

# How does it work?

Internally, ArtDMX starts a worker process for each serial port that it uses for DMX.
A 'master' process receives Artnet from the network and instructs the workers to send
it as DMX. The workers run asyncronously, so they constantly send DMX frames even
if there's no incoming Artnet.

The worker processes use a couple of percent of CPU, even on a Raspberry Pi. The
master can get busy if there's lots of incoming Artnet, something like 7-10% CPU per
universe (on a Raspberry Pi).

# Hardware

This bridge provides serial DMX (only). This means we can use very low-cost RS485
converters in place of expensive DMX interfaces. In my case, I bought the RS485
interfaces for about £2 each, whereas one Entec OpenDMX cost more like £60 (and
more rigorous interfaces are even more expensive).

Incidentally, an Entec OpenDMX is internally made from a USB FTDI chip and an RS485
driver chip. It turns out the cheap devices I bought have the same basic design,
although have (what appear to be) conterfeit FTDI chips (and very poor quality
control). The Entec is undoubtely better made, but isn't that much different
(you can use an Entec OpenDMX as a serial device with ArtDMX if you want).

# Serial and USB Concerns

Running several serial ports at 250K baud requires a bit of finese. I've personally had loads
of problems with USB hubs. I've found that the hubs seem to cause various errors, particularly
when running at high frame rates. A 7 port hub I've used is internally made of two four port
hubs joined together. I can't put more than two serial devices on each hub, or else I get
errors all the time, even at lower frame rates.

The errors that occur are reported in `/var/log/kern.log` and look like these:
```
Jan 11 10:30:06 dmx kernel: [474061.316839] ftdi_sio ttyUSB1: urb failed to clear flow control
Jan 11 10:30:16 dmx kernel: [474071.618851] ftdi_sio ttyUSB5: ftdi_set_termios urb failed to set baudrate
```
