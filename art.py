#!/usr/bin/env python3

import yaml
from argparse import ArgumentParser

from artdmx import main

parser = ArgumentParser()
parser.add_argument("-c", "--config", dest="config", help="Read config Yaml from FILE", metavar="FILE", default="config/artdmx.yaml")
args = parser.parse_args()

with open("config/artdmx.yaml", 'r') as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

universes = dict(config['universes'])

main.start(universes, {})
