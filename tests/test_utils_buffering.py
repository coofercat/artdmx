import unittest

from artdmx.utils import Buffering,BufferOverFlow

class TestBuffering(unittest.TestCase):

    def test_buffering_instantiation(self):
        obj = Buffering()
        self.assertIsInstance(obj, Buffering)

    def test_buffering_returns_line_simple(self):
        obj = Buffering()
        obj.add('hello')
        obj.add('there\n')
        self.assertEqual(obj.buffer, 'hellothere\n')
        self.assertEqual(obj.get(), 'hellothere')
        self.assertEqual(obj.buffer, '')

    def test_buffering_returns_line_complex(self):
        obj = Buffering()
        obj.add('one\ntwo\n')
        self.assertEqual(obj.get(), 'one')
        self.assertEqual(obj.get(), 'two')
        
    def test_buffering_when_buffer_full(self):
        obj = Buffering(10)
        obj.add('hello')
        obj.add('there')
        self.assertRaises(BufferOverFlow, lambda: obj.add('again\n'))
        self.assertEqual(obj.get(), 'hereagain')
